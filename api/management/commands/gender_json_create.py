from django.core.management import BaseCommand
from zpvoy.settings import BASE_DIR
from api.models import Gender
import json

import csv

class Command(BaseCommand):
    help = "This script will create gender txt file"
    def handle(self, *args, **options):
        file = open(BASE_DIR + '/media/gender.json',mode='w',encoding='utf-8')
        g_data = {}
        gen_data = Gender.objects.all()
        if gen_data:
            for gen in gen_data:
                obj = {}
                obj['name'] = str(gen.name)
                obj['gender'] = str(gen.gender)
                obj['accuracy'] = str(gen.accuracy)
                g_data[str(gen.name)] = obj
                # import pdb;pdb.set_trace()
            json.dump(g_data,file)
            print("Complete")
        else:
            print("No data found")