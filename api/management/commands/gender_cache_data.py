from django.core.management import BaseCommand
from django.core.cache import cache
from zpvoy.settings import BASE_DIR
import json

class Command(BaseCommand):
    help = "This script will create gender cache in redis from json file"
    def handle(self, *args, **options):
        file = open(BASE_DIR + '/media/gender.json',mode='r')
        file_obj = json.loads(file.read())
        for key,value in file_obj.items():
            cache_key = 'gender_'+key.lower()
            cache.set(cache_key,value,None)
