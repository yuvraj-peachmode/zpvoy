from django.core.management import BaseCommand
from django.core.cache import cache
from api.models import Apiuser

class Command(BaseCommand):
    help = "This script will add apikey in redis from user data"
    def handle(self, *args, **options):
        user_data = Apiuser.objects.filter()
        for user in user_data:
            if user.api_key and user.api_service_name:
                cache_key = 'apikey_'+user.api_key
                cache.set(cache_key, user.api_service_name.site_name, None)
        print("Done")