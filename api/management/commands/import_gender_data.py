from django.core.management import BaseCommand
from zpvoy.settings import BASE_DIR
from api.models import Gender
from decimal import *

import csv

class Command(BaseCommand):
    help = "This script will add gender data in db using csv"
    def handle(self, *args, **options):
        filename = 'gender.csv'

        with open(BASE_DIR+'/api/management/commands/csv_files/'+filename) as csv_file:
            reader = csv.DictReader(csv_file)
            for row in reader:
                if row['gender'] == 'F':
                    gen ='Female'
                else:
                    gen ='Male'
                Gender.objects.create(name=row['name'],gender=gen,accuracy=Decimal(row['accuracy']))
