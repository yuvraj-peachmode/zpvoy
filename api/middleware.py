from django.utils.deprecation import MiddlewareMixin
from django.http import JsonResponse
from api.models import UserSession
import re
import datetime

class CustomSessionCheck(MiddlewareMixin):
    """
    This class is used to check that user session is available or expired on server side.
    Every time on page load this api is called to that time is user is session is expired then it will be redirect to login page.
    If session is not expired then in this function expiry_time for session will be updated.
    """
    def process_request(self, request):

        if request.path_info not in ['/v1/','/api/users/','/api/login/']:
            key = request.headers.get('Authorization')
            if key and re.findall(r"([a-fA-F\d]{32})", key):
                data = UserSession.objects.filter(session_key=key).last()
                if data:
                    if data.expiry_time > datetime.datetime.now():
                        return
                    else:
                        data.delete()
                        return JsonResponse({'error': 'session expired.'}, status=440)
            else:
                return JsonResponse({'error': 'session-key is invalid.'}, status=440)

            return JsonResponse({'error': 'session-key not found.'}, status=440)
        else:
            # return JsonResponse({'error': 'session-key not found.'}, status=440)
            return

