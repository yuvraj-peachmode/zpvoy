from django.shortcuts import render
from .models import Apiuser, UserSession, ApiServiceName
from .serializers import ApiuserSerializer
from rest_framework import viewsets
from rest_framework.response import Response
from django.contrib.auth.hashers import check_password
from rest_framework.views import APIView
from django.http import JsonResponse
from django.core.cache import cache
from api.decorators import check_domain_access
import hashlib
import datetime
import re
from api.gender import GENDER_DATA

class ApiuserModel(viewsets.ModelViewSet):
    """
    This class is used to create,add,edit and delete user purpose
    To create user
        url : /api/users/
        method : POST
        data : {
            email:test@gmail.com(email,maxlength=70)
            password:TestTest(string,maxlength=100)
            name:Test(string,maxlength=100)
        }

    To get all users
        url : /api/users/
        method : GET

    To get single user
        url : /api/users/{id}/
        method : GET

    To update user
        url : /api/users/{id}/
        method : PUT

    To delete user
        url : /api/users/{id}/
        method : DELETE
    """
    queryset = Apiuser.objects.all()
    serializer_class = ApiuserSerializer

class UserLogin(APIView):
    """
    This class is used to login user
    url : /api/login/
    method : POST
    data : {
        email:test@gmail.com(email,maxlength=70)
        password:TestTest(string,maxlength=100)
    }
    """
    def post(self,request):
        email = request.data.get('email', '')
        password = request.data.get('password', '')
        ser_name = ApiServiceName.objects.filter(site_name = request.META['HTTP_HOST']).last()
        if not ser_name:
            return JsonResponse({'message': "API not available"}, status=400)
        user = authenticate(email=email, password=password, api_service_name=ser_name.id)
        if user is not None:
            user_obj = ApiuserSerializer(user, many=False, context={'request': None})
            user_obj = set_session(user_obj.data)
            return Response(user_obj)
        else:
            return JsonResponse({'message': "Invalid Email or Password."}, status=400)

class UserLogout(APIView):
    """
    This class is used to logout user
    url : /api/logout/
    method : POST
    data : {
        user_id:123(Integer)
        session_key:ec9fd884f7fc81fc9e5596d213b6e827d3109d26(String,encryption_method = MD5)
    }
    """
    def post(self,request):
        session = clear_session(request)
        return JsonResponse({'message': 'User logout successfully.'}, status=200)


def authenticate(email=None, password=None, api_service_name=None):
    """

    :param email:
    :param password:
    :return:
        if user and password  match then return user data else return false

    """
    if email and password and api_service_name:
        user = Apiuser.objects.filter(email=email, api_service_name_id=api_service_name).last()
        if user and check_password(password,user.password):
            return user
    return None

class GenderView(APIView):
    """
    This class is used to get gender data using user name and api_key
    url : /v1/?name=Jaicob&key=ec9fd884f7fc81fc9e5596d213b6e827d3109d26
    method : GET
    """
    @check_domain_access
    def get(self,request):
        key = request.GET.get('key','')
        name = request.GET.get('name','')
        if not key or not name:
            return JsonResponse({'error': 'data not provided.'}, status=400)

        access = check_key(key=key)

        if access:
            data = cache.get('gender_'+str(name).lower())
            if data:
                return Response(data)
            else:
                return JsonResponse({'error': 'name not found.'}, status=200)
        else:
            return JsonResponse({'error': 'Invalid key'}, status=400)

class GetApiKey(APIView):
    def post(self,request):
        user_id = request.data.get('uid', None)
        if user_id:
            user = Apiuser.objects.filter(id=user_id, api_service_name__site_name=request.META['HTTP_HOST']).last()
            if user:
                return JsonResponse({'api_key': user.api_key}, status=200)
        else:
            return JsonResponse({'error': 'Api key not exist.'}, status=400)

        return JsonResponse({'error': 'User not exists.'}, status=400)


def check_key(key=''):
    if key:
        cache_key = 'apikey_'+key
        data = cache.get(cache_key)
        if data:
            return True
    return False

def set_session(user_obj):
    id = user_obj['id']
    email = user_obj['email']
    if id:
        data = UserSession.objects.filter(apiuser_id= id).last()
        if not data:
            session_key = hashlib.md5(str(email).encode()).hexdigest()
            expiry_time = datetime.datetime.now() + datetime.timedelta(days=365)
            data = UserSession.objects.create(apiuser_id= id, session_key=session_key,expiry_time= expiry_time)
        user_obj['session_key'] = data.session_key
    return user_obj

def clear_session(request):
    """
    This function used to clear user session if it is exist else return false
    :param request:
    :return:
    """
    session_key = request.data.get('session_key')
    if not session_key_validation(session_key):
        return False

    if session_key :
        UserSession.objects.filter(session_key=session_key).delete()
        return True
    else:
        return False

def session_key_validation(key=''):
    """
    :param key:
    :return:
        if key exist and validation is proper return True else False
    """
    if key and re.findall(r"([a-fA-F\d]{32})", key):
        return True
    else:
        return False


def get_case_insensitive_key_value(input_dict, key):
    return next((value for dict_key, value in input_dict.items() if dict_key.lower() == key.lower()), None)
