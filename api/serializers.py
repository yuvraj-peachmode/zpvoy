from rest_framework import serializers,status,exceptions
from .models import Apiuser,ApiServiceName,UserSession
from django.contrib.auth.hashers import make_password
from django.core.cache import cache
import hashlib
import datetime

class AlreadyExistException(exceptions.APIException):
    status_code = status.HTTP_409_CONFLICT

class ApiuserSerializer(serializers.ModelSerializer):
    email = serializers.EmailField()
    password = serializers.CharField(write_only=True)
    service_name = serializers.SerializerMethodField(read_only=True)
    session_key = serializers.SerializerMethodField(read_only=True)

    def get_session_key(self, obj):
        if 'session_key' in obj.__dict__.keys():
            return obj.session_key

    class Meta:
        model = Apiuser
        fields = ('id','email', 'password', 'service_name', 'api_key', 'session_key')

    def validate_email(self,data):
        site_name = self.context['request']._request.META['HTTP_HOST']
        obj = Apiuser.objects.filter(email=data, api_service_name__site_name=site_name)
        if obj:
            raise AlreadyExistException("Email already exist.")

        return data

    def create(self, validated_data):
        site_name = self.context['request']._request.META['HTTP_HOST']
        validated_data['password'] = make_password(validated_data['password'])
        validated_data['api_key'] =  hashlib.sha1(validated_data['email'].encode('utf-8')).hexdigest()
        validated_data['api_service_name'] =  ApiServiceName.objects.filter(site_name=site_name).last()

        user_obj = Apiuser.objects.create(**validated_data)
        if user_obj:
            cache_key = 'apikey_'+user_obj.api_key
            cache.set(cache_key, str(user_obj.api_service_name.site_name), None)

        data = UserSession.objects.filter(apiuser_id=user_obj.id).last()
        if not data:
            session_key = hashlib.md5(str(user_obj.email).encode()).hexdigest()
            expiry_time = datetime.datetime.now() + datetime.timedelta(days=365)
            data = UserSession.objects.create(apiuser_id=user_obj.id, session_key=session_key, expiry_time=expiry_time)

        user_obj.session_key = data.session_key
        user_obj.service_name = user_obj.api_service_name.site_name
        return user_obj

    def get_service_name(self,obj):
        reg_on =  obj.api_service_name.site_name if obj and obj.api_service_name else None
        return reg_on
