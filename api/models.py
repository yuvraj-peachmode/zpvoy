from django.db import models

# Create your models here.


class ApiServiceName(models.Model):
    site_name = models.CharField(max_length=50, blank=False, null=False)

class Apiuser(models.Model):
    email = models.EmailField(max_length=70, null=False, blank=False)
    password = models.CharField(max_length=100, blank=False, null=False)
    api_key = models.CharField(max_length=100, blank=True, null=True, default=None)
    api_service_name = models.ForeignKey(ApiServiceName, blank=True, null=True, default=None, on_delete=models.DO_NOTHING, related_name="apiuser_registeredon")

    class Meta:
        app_label = 'api'
        unique_together = ('email','api_service_name')

class UserSession(models.Model):
    session_key = models.CharField(max_length=100, blank=False, null=False)
    apiuser_id = models.IntegerField(blank=False, null=False)
    expiry_time = models.DateTimeField(null=False, blank=False)
