from django.urls import path, include
from rest_framework import routers
from .views import ApiuserModel,UserLogin, UserLogout, GenderView, GetApiKey

router = routers.DefaultRouter()
router.register(r'users', ApiuserModel)

urlpatterns = [
    path('api/login/', UserLogin.as_view() ,name= 'login_api'),
    path('api/logout/', UserLogout.as_view() ,name= 'login_api'),
    path('api/dashboard/', GetApiKey.as_view() ,name= 'dashboard_api'),
    path('api/', include(router.urls)),
    path('v1/', GenderView.as_view(), name='gender_api'),
]