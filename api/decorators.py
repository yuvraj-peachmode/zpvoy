from api.models import Apiuser
from django.http import JsonResponse
from django.core.cache import cache

def check_domain_access(function):
    def wrap(request, *args, **kwargs):
        key = request.request.GET.get('key','')
        domain = request.request.META['HTTP_HOST']
        # check from cache
        check = cache.get('apikey_'+str(key))
        if check == str(domain):
            return function(request, *args, **kwargs)
        else:
            return JsonResponse({'error': 'Invalid Key'}, status=400)
    return wrap